package mx.edu.uthermosillo.a19311009.asesorias

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import mx.edu.uthermosillo.a19311009.asesorias.adapters.OnboardingAdapter
import mx.edu.uthermosillo.a19311009.asesorias.databinding.FragmentInitBinding
import onboarding.FirstFragment
import onboarding.SecondFragment
import onboarding.ThirdFragment


class InitFragment : Fragment() {

    private var _binding: FragmentInitBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentInitBinding.inflate(inflater, container, false)



        val viewPager2 = binding.onboardingViewPager

        val fragmentList = arrayListOf<Fragment>(
            FirstFragment(),
            SecondFragment(),
            ThirdFragment(),
            LoginFragment()        
        )

        val adapter = OnboardingAdapter(fragmentList, requireActivity().supportFragmentManager, lifecycle)
        viewPager2.adapter = adapter

        return binding.root
    }
}