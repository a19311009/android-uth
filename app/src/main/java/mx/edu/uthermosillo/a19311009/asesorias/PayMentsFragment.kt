package mx.edu.uthermosillo.a19311009.asesorias

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mx.edu.uthermosillo.a19311009.asesorias.databinding.FragmentPayMentsBinding


class PayMentsFragment : Fragment() {

    private var _binding: FragmentPayMentsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPayMentsBinding.inflate(inflater, container, false)

        return binding.root
    }


}