package mx.edu.uthermosillo.a19311009.asesorias

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mx.edu.uthermosillo.a19311009.asesorias.databinding.FragmentDatallsBinding


class DatallsFragment : Fragment() {

    private var _binding: FragmentDatallsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDatallsBinding.inflate(inflater, container, false)

        return binding.root
    }


}