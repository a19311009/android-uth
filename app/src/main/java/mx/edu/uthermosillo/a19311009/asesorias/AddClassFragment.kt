package mx.edu.uthermosillo.a19311009.asesorias

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mx.edu.uthermosillo.a19311009.asesorias.databinding.FragmentAddClassBinding


class AddClassFragment : Fragment() {

    private var _binding: FragmentAddClassBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddClassBinding.inflate(inflater, container, false)

        return binding.root
    }


}